import 'package:flutter/material.dart';

showSnackBar({
  required BuildContext context,
  required String msg,
}) {
  return ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(msg),
      duration: Duration(seconds: 3),
    ),
  );
}
