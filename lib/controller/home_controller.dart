import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_count_down_time/blocs/count_down_blocs.dart';
import 'package:fsel_count_down_time/widgets/app_widgets.dart';

import '../blocs/count_down_events.dart';

class HomeController {
  final BuildContext context;
  HomeController({required this.context});

  Timer? _timer;
  int remainSeconds = 1;

  void handleCountDown() {
    final state = context.read<CountDownBloc>().state;
    String time = state.second;

    if(time.isEmpty) {
      showSnackBar(context: context, msg: 'Time can not be empty');
      return;
    }

    print('Time: $time');

    _startTime(int.parse(time));
    
    print('Convert time to String: ${int.parse(time)}');
  }

  _startTime(int time) {
    const duration = Duration(seconds: 1);
    remainSeconds = time;

    _timer = Timer.periodic(duration, (Timer timer) {
      if (remainSeconds == 0) {
        timer.cancel();
      } else {
        int hours = remainSeconds ~/3600;
        int minutes = (remainSeconds % 3600) ~/ 60 ;
        int seconds = remainSeconds % 60;

        String textTime = hours.toString().padLeft(2, '0') + ':' + minutes.toString().padLeft(2, '0') +
            ':' +
            seconds.toString().padLeft(2, '0');

        context.read<CountDownBloc>().add(ConfirmCountDownEvent(textTime));

        print('Count time: $textTime');
        remainSeconds--;
      }
    });
  }
}