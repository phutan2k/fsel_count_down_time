abstract class CountDownEvents {
  const CountDownEvents();
}

class CountDownHomeEvent extends CountDownEvents {
  final String second;

  const CountDownHomeEvent(this.second);
}

class ConfirmCountDownEvent extends CountDownEvents {
  final String time;

  const ConfirmCountDownEvent(this.time);
}

