import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_count_down_time/blocs/count_down_events.dart';
import 'package:fsel_count_down_time/blocs/count_down_states.dart';

class CountDownBloc extends Bloc<CountDownEvents, CountDownStates> {
  CountDownBloc() : super(CountDownStates()) {
    on<CountDownHomeEvent>(_countDownHomeEvent);
    on<ConfirmCountDownEvent>(_confirmCountDownEvent);
  }

  void _countDownHomeEvent(
      CountDownHomeEvent event, Emitter<CountDownStates> emit) {
    print('Time: ${state.second}');
    emit(state.copyWith(second: event.second));
  }

  void _confirmCountDownEvent(
      ConfirmCountDownEvent event, Emitter<CountDownStates> emit) {
    print('Time: ${state.time}');
    emit(state.copyWith2(time: event.time));
  }
}
