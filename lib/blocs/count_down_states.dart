class CountDownStates {
  final String second;
  final String time;

  const CountDownStates({
    this.second = '',
    this.time = '',
  });

  CountDownStates copyWith({
    String? second,
  }) {
    return CountDownStates(
      second: second ?? this.second,
    );
  }

  CountDownStates copyWith2({
    String? time,
  }) {
    return CountDownStates(
      time: time ?? this.time,
    );
  }
}
