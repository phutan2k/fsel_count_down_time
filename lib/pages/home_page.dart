import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_count_down_time/blocs/count_down_blocs.dart';
import 'package:fsel_count_down_time/blocs/count_down_events.dart';
import 'package:fsel_count_down_time/blocs/count_down_states.dart';
import 'package:fsel_count_down_time/controller/home_controller.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String time = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CountDownBloc, CountDownStates>(
        builder: (context, state) {
          return Container(
            color: Colors.white,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 60,
                          child: TextField(
                            onChanged: (value) => {
                              print(value),
                              context.read<CountDownBloc>().add(CountDownHomeEvent(value))
                            },
                            decoration: InputDecoration(
                              hintText: 'input',
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Avenir',
                              fontWeight: FontWeight.normal,
                              fontSize: 14,
                            ),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      InkWell(
                        onTap: () {
                          HomeController(context: context).handleCountDown();
                          // context.read<CountDownBloc>().add(ConfirmCountDownEvent(time));
                        },
                        child: Container(
                          width: 60,
                          height: 60,
                          padding: EdgeInsets.all(20),
                          child: Text(
                            'OK',
                            style: TextStyle(color: Colors.white),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.green,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Text(
                      context.read<CountDownBloc>().state.time,
                      style: TextStyle(fontSize: 25),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
